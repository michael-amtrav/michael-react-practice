import React from 'react';
import ReactDOM from 'react-dom';
import App from './container-components/AppContainer';
import registerServiceWorker from './registerServiceWorker';


const appData = {
    appName: 'Star Wars Lookup',
    lookupOptions:[
        {lookupText:'People', urlText:'people'}, 
        {lookupText:'Planets', urlText:'planets'},
        {lookupText: 'Starships', urlText: 'starships'},
        {lookupText: 'Vehicles', urlText: 'vehicles' },
        {lookupText: 'Films', urlText: 'films' },
        {lookupText: 'Species', urlText: 'species' },
    ],
    swapiURL:'https://swapi.co/api/',
};

ReactDOM.render(<App appData={appData} />, document.getElementById('root'));
registerServiceWorker();
