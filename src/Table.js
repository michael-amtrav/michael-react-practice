import React, { Component } from 'react';
import ErrorMessage from './ErrorMessage';
import AttributeRow from './AttributeRow';
import ValueRow from './ValueRow';


// TODO: Pull out helper functions into helpers; make a view component that receives prepared data; container-component should send prepared data
class Table extends Component {

    render() {
        if (this.props.isError) {
            return <ErrorMessage />
        }

        else {
            var attributes = Object.keys(this.props.result).map( (el) => el.toUpperCase() );
            var values = Object.values(this.props.result);
            return (
                <table className="table table-striped star-wars-table">
                    <tbody>
                        <AttributeRow attributes={attributes} />
                        <ValueRow values={values} />
                    </tbody>
                </table>
            );

        }
    }
}


export default Table;