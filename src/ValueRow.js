import React, { Component } from 'react';

// TODO: Pull out helper functions into helpers; make a view component that receives prepared data; container-component should send prepared data
class ValueRow extends Component {
    render() {

        let formattedVals = this.props.values.map((el) => {
            if (typeof el === "object") {
                try {
                    el = Object.values(el).join('\n');
                }
                catch (err) {
                    console.log('err', err);
                }

            }
            return el;
        });

        return (
            <tr key={this.props.index}>
                {
                    formattedVals.map((el, index) => {
                        return (
                            <td className="column" key={index}>
                                <code>{el}</code>
                            </td>
                        );
                    })
                }
            </tr>
        );
    }
}

export default ValueRow;