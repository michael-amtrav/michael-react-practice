import React, { Component } from 'react';

class AttributeRow extends Component {
    render() {
        return (
            <tr>
                {this.props.attributes.map(function (el, index) {
                    return <td className="column" key={index}>{el}</td>
                })}
            </tr>
        );
    }
}

export default AttributeRow;