import React, { Component } from 'react';

class ErrorMessage extends Component {
    render() {
        return (
            <div className="alert alert-danger text-center" role="alert">
                Sorry!  An error occurred.  Please try again!
      </div>
        );
    }
}

export default ErrorMessage;