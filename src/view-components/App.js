import React from 'react';
// import Header from './Header';
// import Table from './Table';
import './App.css';

const App = props => (
  <div className="container-fluid">
    <Header appName={props.appData.appName} />
    <RequestForm
      lookupOptions={props.appData.lookupOptions}
      handleSelectChanged={props.handleSelectChanged}
      handleInputChanged={props.handleInputChanged}
      handleSubmitClicked={props.handleSubmitClicked}
      requestNumber={props.requestNumber}
      lookupType={props.lookupType}
    />
    <Table result={props.result} isError={props.isError} />
  </div>
);

export default App;