import React, { Component } from 'react';
import moment from 'moment';
import App from '../view-components/App';

class AppContainer extends Component {

  constructor(props){
    super(props);

    this.state = {
      lookupType:'',
      requestNumber:'',
      result:{},
      isError:false
    };

  }

  handleSelectChanged = (e) => {
    var selectedVal = e.currentTarget.value;
    this.setState({
      lookupType:selectedVal
    });
  }

  handleInputChanged = (e) => {
    var reqNum = e.currentTarget.value;

    this.setState({
      requestNumber:reqNum
    });
  }

  handleSubmitClicked = (e) => {
    e.preventDefault();

    // TODO: move to webservices
    let url = `${this.props.appData.swapiURL}/${this.state.lookupType}/${this.state.requestNumber}`;

    axios.get(url).then(
      (response) => {
        this.setState({
          result: response.data,
          isError:false
        });
      },
      (err) => {
        console.log('err', err);
        this.setState({
          isError:true
        })
      }
    )
    .catch(err => console.error(err));
    
  }

  render() {
    const preparedProps = {
      appName: this.props.appData.appName,
      lookupOptions: this.props.appData.lookupOptions,
      handleInputChanged: this.handleInputChanged,

    };

    return <App {...preparedProps} />;
  }
}


// TODO: move to separate file
class RequestForm extends Component {

  render(){

    return (
      <div className="row">
        <div className="col-sm"></div>
          <form className="input-group-append">
            <select className="custom-select" onChange={this.props.handleSelectChanged}>
              <option value="">Please Select</option>
              {this.props.lookupOptions.map((optionObj, index, arr) => {
                return <option key={index} value={optionObj.urlText}>{optionObj.lookupText}</option>
              })}
            </select>

            <input type="number" className="form-control" onChange={this.props.handleInputChanged} value={this.props.requestNumber} min="1"/>
            <button className="btn btn-primary" type="submit" onClick={this.props.handleSubmitClicked} disabled={!this.props.requestNumber.length || !this.props.lookupType}>Request</button>
          </form>
        <div className="col-sm"></div>
      </div>
    );
  }
}

export default App;